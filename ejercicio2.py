f=open(input("ingrese el nombre del archivo: "))

c = 0
for linea in f:
    linea = linea.rstrip()
    if linea == "": continue

    palabra = linea.split()
    if palabra[0] != "From": continue

    print(palabra[1])
    c = c + 1

print("Hay", c, "lineas en el archivo con la palabra From al inicio")